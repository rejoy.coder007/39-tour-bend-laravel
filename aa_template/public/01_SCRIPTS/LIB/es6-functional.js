const forEach = (array,fn) => {
    let i;
    for(i=0;i<array.length;i++)
        fn(array[i]);
};


const forEachObject = (obj,fn) => {
    for (var property in obj) {
        if (obj.hasOwnProperty(property)) {
            //calls the fn with key and value as its argument
            fn(property, obj[property]);
        }
    }
};



const unless = (predicate,fn) => {
    if(!predicate)
        fn();
};


const times = (times, fn) => {
    for (var i = 0; i < times; i++) fn(i);
};


const every = (arr,fn) => {
    let result = true;
    for(const value of arr)
        result = result && fn(value);
    return result;
};


const some = (arr,fn) => {
    let result = false;
    for(const value of arr)
        result = result || fn(value);
    return result;
};

const sortBy = (property) => {
    return (a,b) => {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result;
    };
};

export {forEach,forEachObject,unless,times,every,some,sortBy};
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJlczYtZnVuY3Rpb25hbC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBmb3JFYWNoID0gKGFycmF5LGZuKSA9PiB7XG4gICAgbGV0IGk7XG4gICAgZm9yKGk9MDtpPGFycmF5Lmxlbmd0aDtpKyspXG4gICAgICAgIGZuKGFycmF5W2ldKTtcbn07XG5cblxuY29uc3QgZm9yRWFjaE9iamVjdCA9IChvYmosZm4pID0+IHtcbiAgICBmb3IgKHZhciBwcm9wZXJ0eSBpbiBvYmopIHtcbiAgICAgICAgaWYgKG9iai5oYXNPd25Qcm9wZXJ0eShwcm9wZXJ0eSkpIHtcbiAgICAgICAgICAgIC8vY2FsbHMgdGhlIGZuIHdpdGgga2V5IGFuZCB2YWx1ZSBhcyBpdHMgYXJndW1lbnRcbiAgICAgICAgICAgIGZuKHByb3BlcnR5LCBvYmpbcHJvcGVydHldKTtcbiAgICAgICAgfVxuICAgIH1cbn07XG5cblxuXG5jb25zdCB1bmxlc3MgPSAocHJlZGljYXRlLGZuKSA9PiB7XG4gICAgaWYoIXByZWRpY2F0ZSlcbiAgICAgICAgZm4oKTtcbn07XG5cblxuY29uc3QgdGltZXMgPSAodGltZXMsIGZuKSA9PiB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aW1lczsgaSsrKSBmbihpKTtcbn07XG5cblxuY29uc3QgZXZlcnkgPSAoYXJyLGZuKSA9PiB7XG4gICAgbGV0IHJlc3VsdCA9IHRydWU7XG4gICAgZm9yKGNvbnN0IHZhbHVlIG9mIGFycilcbiAgICAgICAgcmVzdWx0ID0gcmVzdWx0ICYmIGZuKHZhbHVlKTtcbiAgICByZXR1cm4gcmVzdWx0O1xufTtcblxuXG5jb25zdCBzb21lID0gKGFycixmbikgPT4ge1xuICAgIGxldCByZXN1bHQgPSBmYWxzZTtcbiAgICBmb3IoY29uc3QgdmFsdWUgb2YgYXJyKVxuICAgICAgICByZXN1bHQgPSByZXN1bHQgfHwgZm4odmFsdWUpO1xuICAgIHJldHVybiByZXN1bHQ7XG59O1xuXG5jb25zdCBzb3J0QnkgPSAocHJvcGVydHkpID0+IHtcbiAgICByZXR1cm4gKGEsYikgPT4ge1xuICAgICAgICB2YXIgcmVzdWx0ID0gKGFbcHJvcGVydHldIDwgYltwcm9wZXJ0eV0pID8gLTEgOiAoYVtwcm9wZXJ0eV0gPiBiW3Byb3BlcnR5XSkgPyAxIDogMDtcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9O1xufTtcblxuZXhwb3J0IHtmb3JFYWNoLGZvckVhY2hPYmplY3QsdW5sZXNzLHRpbWVzLGV2ZXJ5LHNvbWUsc29ydEJ5fTsiXSwiZmlsZSI6ImVzNi1mdW5jdGlvbmFsLmpzIn0=
