<header>


    <div class="navigation">
        <input type="checkbox" class="navigation__checkbox" id="navi-toggle">

        <label for="navi-toggle" class="navigation__button">
            <span class="navigation__icon">&nbsp;</span>
        </label>

        <div class="navigation__background">&nbsp;</div>

        <nav class="navigation__nav">
            <ul class="navigation__list">
                <li class="navigation__item"><a href="#" class="navigation__link"><span>01</span>Item 1</a></li>
                <li class="navigation__item"><a href="#" class="navigation__link"><span>02</span>Item 2</a></li>
                <li class="navigation__item"><a href="#" class="navigation__link"><span>03</span>Item 3</a></li>
                <li class="navigation__item"><a href="#" class="navigation__link"><span>04</span>Item 4</a></li>
                <li class="navigation__item"><a href="#" class="navigation__link"><span>05</span>Item 5</a></li>
            </ul>
        </nav>
    </div>


    {{--

       <div class="top-nav container">
        <div class="logo">
            <img src="http://wayanadtoursandtravels.com/02_IMAGES/favicon.png" alt="Logo" class="header__logo">

        </div>
        <ul>
            <li><a href="#">About</a></li>
            <li><a href="#">Testimony</a></li>
            <li><a href="#">Best Products</a></li>
            <li><a href="#">New Arrival</a></li>
        </ul>
    </div> <!-- end top-nav -->

    --}}

    <div class="hero container">
        <div class="hero-copy">
            <h1>Hardware Shop</h1>
            <p>Quality Product</p>
            {{--
            <div class="hero-buttons">
                <a href="#" class="button button-white">Button 1</a>
                <a href="#" class="button button-white">Button 2</a>
            </div>--}}
        </div> <!-- end hero-copy -->

        <div class="hero-image">
            <img src="img/aa_header/1.jpg" alt="hero image">
        </div>
    </div> <!-- end hero -->










</header>
<div class="featured-section">
    <div class="container">



       <h1 class="text-center"> Products</h1>



       <p class="section-description text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid earum fugiat debitis nam, illum vero, maiores odio exercitationem quaerat. Impedit iure fugit veritatis cumque quo provident doloremque est itaque.</p>

       <div class="text-center button-container">
           <a href="#" class="button">Featured</a>
           <a href="#" class="button">On Sale</a>
       </div>


        <div class="products text-center">
            <div class="product">
                <a href="#"><img src="img/aa_header/1.jpg" alt="product"></a>
                <a href="#"><div class="product-name">MacBook Pro</div></a>
                <div class="product-price">$2499.99</div>
            </div>
            <div class="product">
                <a href="#"><img src="img/aa_header/1.jpg" alt="product"></a>
                <a href="#"><div class="product-name">MacBook Pro</div></a>
                <div class="product-price">$2499.99</div>
            </div>
            <div class="product">
                <a href="#"><img src="img/aa_header/1.jpg" alt="product"></a>
                <a href="#"><div class="product-name">MacBook Pro</div></a>
                <div class="product-price">$2499.99</div>
            </div>
            <div class="product">
                <a href="#"><img src="img/aa_header/1.jpg" alt="product"></a>
                <a href="#"><div class="product-name">MacBook Pro</div></a>
                <div class="product-price">$2499.99</div>
            </div>
            <div class="product">
                <a href="#"><img src="img/aa_header/1.jpg" alt="product"></a>
                <a href="#"><div class="product-name">MacBook Pro</div></a>
                <div class="product-price">$2499.99</div>
            </div>
            <div class="product">
                <a href="#"><img src="img/aa_header/1.jpg" alt="product"></a>
                <a href="#"><div class="product-name">MacBook Pro</div></a>
                <div class="product-price">$2499.99</div>
            </div>
            <div class="product">
                <a href="#"><img src="img/aa_header/1.jpg" alt="product"></a>
                <a href="#"><div class="product-name">MacBook Pro</div></a>
                <div class="product-price">$2499.99</div>
            </div>
            <div class="product">
                <a href="#"><img src="img/aa_header/1.jpg" alt="product"></a>
                <a href="#"><div class="product-name">MacBook Pro</div></a>
                <div class="product-price">$2499.99</div>
            </div>
        </div> <!-- end products -->

        <div class="text-center button-container">
            <a href="#" class="button">View more products</a>
        </div>

    </div> <!-- end container -->

</div> <!-- end featured-section -->


<div class="testimony-section">
    <div class="container">
        <h1 class="text-center">Testimonials</h1>

        <p class="section-description text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et sed accusantium maxime dolore cum provident itaque ea, a architecto alias quod reiciendis ex ullam id, soluta deleniti eaque neque perferendis.</p>

        <div class="testimony-posts">
            <div class="blog-post" id="testimony1">
                <a href="#"><img src="img/aa_header/1.jpg" alt="blog image"></a>
                <a href="#"><h2 class="blog-title">Testimonials 1 </h2></a>
                <div class="blog-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est ullam, ipsa quasi?</div>
            </div>
            <div class="blog-post" id="testimony2">
                <a href="#"><img src="img/aa_header/1.jpg" alt="blog image"></a>
                <a href="#"><h2 class="blog-title">Testimonials 2</h2></a>
                <div class="blog-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est ullam, ipsa quasi?</div>
            </div>
            <div class="blog-post" id="testimony3">
                <a href="#"><img src="img/aa_header/1.jpg" alt="blog image"></a>
                <a href="#"><h2 class="blog-title">Testimonials 3</h2></a>
                <div class="blog-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est ullam, ipsa quasi?</div>
            </div>
        </div> <!-- end blog-posts -->
    </div> <!-- end container -->
</div> <!-- end testimony-section -->




<footer>
    <div class="footer-content container">
        <div class="made-with">Made for Demo <i class="fa fa-heart"></i> by Group Name</div>
        <ul>
            <li>Follow US:</li>
            <li><a href="#"><i class="fa fa-globe"></i></a></li>
            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
            <li><a href="#"><i class="fa fa-github"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
        </ul>
    </div> <!-- end footer-content -->
</footer>