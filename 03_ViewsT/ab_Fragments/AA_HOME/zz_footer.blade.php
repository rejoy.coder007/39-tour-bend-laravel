


<footer>
    <div class="footer-content container">
        <div class="made-with">Made for Demo <i class="fa fa-heart"></i> by Group Name</div>
        <ul>
            <li>Follow US:</li>
            <li><a href="#"><i class="fa fa-globe"></i></a></li>
            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
            <li><a href="#"><i class="fa fa-github"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
        </ul>
    </div> <!-- end footer-content -->
</footer>